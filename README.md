CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The entity autocomplete anchor module provides a widget that allows users to add
anchors to links set using the entity autocomplete widget.

Problem: when using a link field with the entity autocomplete widget you aren't
able to provide an anchor (hash part, or fragment) to the resulting url anymore.

Solution: when the user selects an entity reference a new field for the anchor
is shown.
The result URL is: /node/1<strong>#section</strong>

If you are using the https://www.drupal.org/project/link_attributes module and
want to add links to specific paragraphs, this module allows you to do that
without hardcoding the urls.

The module provides two implementations:
1. as a separate widget (Link with anchor)
2. as a hook that alters the default link widget

The widget is provided for edge cases when the default link widget cannot be used.
It works the same but is not recommended because it extends the Link Widget class and
overwrites the formElement method. If the overwritten method is updated in a future
core release, the link widget provided by this module will not be updated automatically.
For these type of situations the hook implementation should be toggled using a config
(to be implemented in a future release).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/entity_autocomplete_anchor


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-8 for
   further information.


CONFIGURATION
-------------

The module provides a settings page where you can select the entity types for which the anchor support will be added to link widgets.

In order to use this functionality, follow the following steps:

 * Enable the module like normal
 * Select the entity types for which it will be available (by default it is enabled for Nodes and Menu links)
 * The new field will appear on *link* fields when selecting an entity reference using the autocomplete
 * For edge cases (see above), edit the widget using 'Manage form display'
   and select the 'Link with anchor' widget


MAINTAINERS
-----------

 * Bogdan Dinu (bogdan.dinu) - https://www.drupal.org/u/bogdandinu
