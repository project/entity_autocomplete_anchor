<?php

namespace Drupal\entity_autocomplete_anchor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_autocomplete_anchor_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('entity_autocomplete_anchor.settings');
    $options = $config->get('entity_autocomplete_anchor.entity_types');

    $types = [];
    foreach(\Drupal::entityTypeManager()->getDefinitions() as $key => $def) {
      if(!$def->getBundleOf() && $def->getGroup() == 'content')
      $types[$key] = $def->getLabel();
      $selected_options[$key] = in_array($key, $options) ? '1' : '0';
    }

    foreach($options as $option) {
      if(!in_array($option, array_keys($types))) {
        $types[$option] = $option . ' (MISSING)';
      }
    }
    $form['title'] = [
      '#type' => 'markup',
      '#markup' => '<h2>' . $this->t('Entity Autocomplete Anchor settings') . '</h2>',
    ];
  
    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity Types'),
      '#options' => $types,
      '#default_value' => $options,
      '#description' => $this->t('Select the entity types for which to enable the anchor on link fields'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_autocomplete_anchor.settings');
    $config->set('entity_autocomplete_anchor.entity_types', array_filter(array_values($form_state->getValue('entity_types'))));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_autocomplete_anchor.settings',
    ];
  }
}