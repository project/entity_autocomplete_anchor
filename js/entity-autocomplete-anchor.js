(function($) {
  $.fn.linkAnchorCallback = function(selector, show) {
    if($(selector)) {
      if(show) {
        $(selector).show();
      }
      else {
        $(selector).find('input').val('');
        $(selector).hide();
      }
    }
  };
})(jQuery);